public class Quadrato extends Forma {
    private double lato;

    public Quadrato(Colore colore, double lato) {
        super(colore);
        this.lato = lato;
    }

    @Override
    public double getArea() {
        return lato*lato;
    }

    @Override
    public double getPerimetro() {
        return lato*4;
    }

    @Override
    public String toString() {
        return "Quadrato{" +
                "lato=" + lato + super.toString() +
                '}';
    }
}
