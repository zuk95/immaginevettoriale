import java.util.Arrays;

public class ImgVect {
    private int maxelem,n;
    private Forma elementi[];

    public ImgVect(int maxelem) {
        this.maxelem = maxelem;
        n=0;
        this.elementi= new Forma[maxelem];
    }

    public void addComponente(Forma forma){
        elementi[n]=forma;
        n++;
    }

    public double getSommaAree(){
        double tot=0;
        for(int i=0;i<n;i++){
            tot = tot + elementi[i].getArea();
        }
        return tot;
    }

    public double getSommaAree(Colore colore){
        double tot=0;
        for(int i=0;i<n;i++){
            if(colore == elementi[i].getColore()){
                tot=tot+elementi[i].getArea();
            }
        }
        return tot;
    }

    @Override
    public String toString() {
        return "ImgVect{" +
                "elementi=" + Arrays.toString(elementi) +
                '}';
    }

    public void ordinaForme(){
        Arrays.sort(elementi,0,n);
    }

}
