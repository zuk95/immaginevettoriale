public class Tester {
    public static void main(String[] args) {
        Forma ret1 = new Rettangolo(Colore.BLACK,5,5);
        Forma quad1 = new Quadrato(Colore.RED,10);
        Forma cerc1 = new Cerchio(Colore.YELLOW,2);

        ImgVect imm = new ImgVect(5);
        imm.addComponente(ret1);
        imm.addComponente(quad1);
        imm.addComponente(cerc1);

        System.out.println(imm.toString());

        System.out.println("Aree BLACK: " +imm.getSommaAree(Colore.BLACK));
        System.out.println("Aree YELLOW: "+imm.getSommaAree(Colore.YELLOW));
        System.out.println("Aree RED: "+imm.getSommaAree(Colore.RED));
        System.out.println("Aree BLUE: "+imm.getSommaAree(Colore.BLUE));

        System.out.println(ret1.compareTo(quad1));

        imm.ordinaForme();
        System.out.println(imm.toString());


    }
}
