public class Cerchio extends Forma {
    private double raggio;

    public Cerchio(Colore colore, double raggio) {
        super(colore);
        this.raggio = raggio;
    }

    @Override
    public double getArea() {
        return (raggio*raggio)*Math.PI;
    }

    @Override
    public double getPerimetro() {
        return 2*Math.PI*raggio;
    }

    @Override
    public String toString() {
        return "Cerchio{" +
                "raggio=" + raggio + super.toString() +
                '}';
    }
}
