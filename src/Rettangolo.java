public class Rettangolo extends Forma{
    private double base,altezza;

    public Rettangolo(Colore colore, double base, double altezza) {
        super(colore);
        this.base = base;
        this.altezza = altezza;
    }

    @Override
    public double getArea() {
        return base*altezza;
    }

    @Override
    public double getPerimetro() {
        return (base+altezza)*2;
    }

    @Override
    public String toString() {
        return "Rettangolo{" +
                "base=" + base +
                ", altezza=" + altezza + super.toString()+
                '}';
    }
}
