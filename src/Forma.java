public abstract class Forma implements Comparable {
    private Colore colore;

    public Forma(Colore colore) {
        this.colore = colore;
    }

    public Colore getColore() {
        return colore;
    }

    public abstract double getArea();
    public abstract double getPerimetro();

    @Override
    public String toString() {
        return " colore=" + colore + " Area: "+ getArea();
    }

    @Override
    public int compareTo(Object o) {
        Forma forma = (Forma)o;
        return (int)(this.getArea() - forma.getArea());
    }
}
